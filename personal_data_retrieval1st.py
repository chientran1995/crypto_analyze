import os
import yaml
import time
import argparse

from pybinance_utils import common, get_balances, get_orders, get_trades, get_market_prices


parser = argparse.ArgumentParser(description='Process arguments.')
parser.add_argument('-conf', '--config', type=str, required=True, help='Config file with api key and api secret.')
parser.add_argument('-first', '--isFirst', action='store_true',
                    help='Use when get data for the first time. start_time is set to 2017-01-01 00:00:00 if this param is specified')
parser.add_argument('-stime', '--startTime', type=str, required=False, help='Start time to get data with format YYYY-mm-dd HH:MM:SS')
parser.add_argument('-etime', '--endTime', type=str, required=True, help='End time to get data with format YYYY-mm-dd HH:MM:SS')
args = parser.parse_args()

def main(args):
    config_file = args.config
    first_run = args.isFirst
    if first_run:
        start_time = '2017-01-01 00:00:00'
    else:
        start_time = args.startTime
        if not start_time:
            print('startTime is required if isFirst is False')
            return
    end_time = args.endTime
    
    with open(config_file, 'r') as conf_file:
        cfg = yaml.load(conf_file, Loader=yaml.FullLoader)
    api_key, api_secret = cfg['API']['api_key'], cfg['API']['api_secret']

    ### Get personal data
    personal_data_dir = 'data/personal_data'
    if not os.path.exists(personal_data_dir):
        os.makedirs(personal_data_dir)

    spot_df = get_balances.get_spot_balance(api_key, api_secret)
    saving_df = get_balances.get_earn_balance(api_key, api_secret)
    saving_hist_df = get_balances.get_earn_interest_history(api_key, api_secret, startTime=start_time, endTime=end_time)
    print('Done getting balance data')

    order_hist_df = get_orders.get_user_orders(api_key, api_secret, startTime=None, endTime=end_time)
    open_order_df = get_orders.get_user_open_orders(api_key, api_secret)
    print('Done getting order data')
    time.sleep(60)

    trade_hist_df = get_trades.get_user_trades(api_key, api_secret, startTime=None, endTime=end_time)
    deposit_hist_df = get_trades.get_user_deposit(api_key, api_secret, startTime=start_time, endTime=end_time)
    saving_subscription_df = get_trades.get_saving_purchase(api_key, api_secret, startTime=start_time, endTime=end_time)
    saving_redemption_df = get_trades.get_saving_redemption(api_key, api_secret, startTime=start_time, endTime=end_time)
    print('Done getting trade data')

    spot_df.to_csv('{}/balance_spot.csv'.format(personal_data_dir), index=False)
    saving_df.to_csv('{}/balance_saving.csv'.format(personal_data_dir), index=False)
    saving_hist_df.to_csv('{}/balance_saving_history.csv'.format(personal_data_dir), index=False)
    order_hist_df.to_csv('{}/order_history_spot.csv'.format(personal_data_dir), index=False)
    open_order_df.to_csv('{}/order_opening_spot.csv'.format(personal_data_dir), index=False)
    trade_hist_df.to_csv('{}/trade_history_spot.csv'.format(personal_data_dir), index=False)
    deposit_hist_df.to_csv('{}/trade_deposit_history_spot.csv'.format(personal_data_dir), index=False)
    saving_subscription_df.to_csv('{}/trade_saving_subscription_spot.csv'.format(personal_data_dir), index=False)
    saving_redemption_df.to_csv('{}/trade_saving_redemption_spot.csv'.format(personal_data_dir), index=False)
    time.sleep(60)

if __name__ == '__main__':
    main(args)
    