# Crypto repository

## Usage instructions

#### Binance utils (binance_utils.py)
- get_binance_pair: Get all pair from Binance or filter based on reference coin
    - Params:
        - ref_coin (str, optional): Reference coin. Defaults to None to get all pairs.
    - Example:
        - get_binance_pair(ref_coin='usdt')
- get_binance_price: Get candlestick price of symbol (pair of coin) from Binance
    - Params:
        - symbol (str): Symbol of coin pair
        - interval (str, optional): Timeframe of each candle. Defaults to '1d'.
        - limit (int, optional): Maximum number of candle to get. Defaults to 500.
        - startTime (str, optional): Start time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None.
        - endTime (str, optional): End time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None.
    - Example:
        - df = get_binance_price('btcusdt', interval='1h', startTime='2021-05-01 00:00:00', endTime='2021-05-02 23:59:59')
        - df = get_binance_price('btcusdt', interval='1h', limit=200)

#### Technical indicator (TA_cal.py)
- bollingerbands_calculation
- rsi_calculation
- macd_calculation
- ichimoku_calculation