import numpy as np
import pandas as pd

def bollingerbands_calculation(stockprice:pd.Series, window_size=20, times_of_std=2) -> pd.DataFrame:
    """Function to calculate Bollinger bands
    
    Args:
        stockprice (pd.Series): Pandas series of stockprice (usually use close price of market)
        window_size (int, optional): Number of observation days (default: 20)
        times_of_std (int, optional): Multiplier of standard deviation (default: 2)
    
    Returns:
        pd.DataFrame: Pandas DataFrame of Bollinger bands elements as columns: rolling_mean, upper_band, lower_band
    """
    rolling_mean = stockprice.rolling(window=window_size).mean()
    rolling_std  = stockprice.rolling(window=window_size).std()
    upper_band = rolling_mean + (rolling_std*times_of_std)
    lower_band = rolling_mean - (rolling_std*times_of_std)
    result = pd.DataFrame({'rolling_mean': rolling_mean, 'upper_band': upper_band, 'lower_band': lower_band})
    return result

def rsi_calculation(stockprice:pd.Series, window_size=14) -> pd.DataFrame:
    """Function to calculate RSI
    
    Args:
        stockprice (pd.Series): Pandas series of stockprice (usually use close price of market)
        window_size (int, optional): Number of observation days (default: 14)
    
    Returns:
        pd.DataFrame: Pandas DataFrame of RSI elements as columns: rsi
    """
    deltas = np.diff(stockprice)
    seed = deltas[:window_size+1]
    up = seed[seed>=0].sum()/window_size
    down = -seed[seed<0].sum()/window_size
    rs = up/down
    rsi = np.zeros_like(stockprice)
    rsi[:window_size] = 100. - 100./(1.+rs)

    for i in range(window_size, len(stockprice)):
        delta = deltas[i-1]
        if delta>0:
            upval = delta
            downval = 0.
        else:
            upval = 0.
            downval = -delta

        up = (up*(window_size-1) + upval)/window_size
        down = (down*(window_size-1) + downval)/window_size
        rs = up/down
        rsi[i] = 100. - 100./(1.+rs)
        
    result = pd.DataFrame({'rsi': rsi})
    return result

def macd_calculation(stockprice:pd.Series, fast_length=12, slow_length=26, signal_length=9) -> pd.DataFrame:
    """Function to calculate MACD
    
    Args:
        stockprice (pd.Series): Pandas series of stockprice (usually use close price of market)
        fast_length (int, optional): Lower number of observation days (default: 12)
        slow_length (int, optional): Higher number of observation days (default: 26)
        signal_length (int, optional): Observation days to calculate signal line (default: 9)
    
    Returns:
        pd.DataFrame: Pandas DataFrame of MACD elements as columns: macd, signal, macd_histogram
    """
    ema12 = stockprice.ewm(span=fast_length, adjust=False).mean()
    ema26 = stockprice.ewm(span=slow_length, adjust=False).mean()
    macd = ema12 - ema26
    signal = macd.ewm(span=signal_length, adjust=False).mean()
    macd_histogram = macd - signal
    result = pd.DataFrame({'macd': macd, 'signal': signal, 'macd_histogram': macd_histogram})
    return result

def ichimoku_calculation(df:pd.DataFrame, conversion_period=9, baseline_period=26, lagging_period=52, displacement=26) -> pd.DataFrame:
    """Function to calculate Ichimoku cloud
    
    Args:
        df (pd.DataFrame): Pandas DataFrame requires columns 'High', 'Low', 'Closeprice' to compute Ichimoku elements
        conversion_period (int, optional): Number of periods to calculate Tenkan-sen (default: 9)
        baseline_period (int, optional): Number of periods to calculate Kijun-sen and Chikou-span (default: 26)
        lagging_period (int, optional): Number of periods to calculate Senkou-span-B (default: 52)
        displacement (int, optional): Periods in the future of Senkou-span-A and Senkou-span-B (default: 26)
    
    Returns:
        pd.DataFrame: Pandas DataFrame of Ichimoku cloud elements as columns: tenkan_sen, kijun_sen, chikou_span, senkou_span_a, senkou_span_b
    """
    ichimoku_df = pd.DataFrame({})
    high_conversion = df['High'].rolling(window=conversion_period).max()
    low_conversion = df['Low'].rolling(window=conversion_period).min()
    ichimoku_df['tenkan_sen'] = (high_conversion + low_conversion) /2
    
    high_baseline = df['High'].rolling(window=baseline_period).max()
    low_baseline = df['Low'].rolling(window=baseline_period).min()
    ichimoku_df['kijun_sen'] = (high_baseline + low_baseline) /2
    
    ichimoku_df['chikou_span'] = df['Closeprice'].shift(-baseline_period)
    
    extend_vals = [np.NaN for _ in range(displacement)]
    extend_df = pd.DataFrame({'tenkan_sen': extend_vals, 'kijun_sen': extend_vals, 'chikou_span': extend_vals})
    ichimoku_df = ichimoku_df.append(extend_df, ignore_index=True)
    
    ichimoku_df['senkou_span_a'] = ((ichimoku_df['tenkan_sen'] + ichimoku_df['kijun_sen']) / 2).shift(displacement)

    high_lagging = df['High'].rolling(window=lagging_period).max()
    low_lagging = df['Low'].rolling(window=lagging_period).min()
    ichimoku_df['senkou_span_b'] = ((high_lagging + low_lagging) /2)
    ichimoku_df['senkou_span_b'] = ichimoku_df['senkou_span_b'].shift(displacement)
    
    return ichimoku_df
