import os
import time
import requests
import numpy as np
import pandas as pd

from binance import Client

from .common import epochTime2dt, dt2epochTime


BINANCE_START_DATE_STR = '1 Jan, 2017'
BINANCE_START_DATE_DT = '2017-01-01 00:00:00'

def get_all_pair_prices(data_save_dir:str, interval:str, startTime:str, endTime:str=None, isFirst:bool=False) -> list:
    """Get candlestick market price of all used pair

    Args:
        data_save_dir (str): Location to save dataframes
        interval (str): Timeframe of each candle.
        startTime (str): Start time to get data (Recommend format YYYY-mm-dd). Data will be retrieved from 07:00:00 of that date
        endTime (str, optional): End time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None to get until now
        isFirst (bool, optional): Use when get data for the first time. startTime is set to 2017-01-01 00:00:00 if this param is True. Defaults to False.

    Returns:
        list: List of saved name of DataFrames to data/ directory in format symbol/interval_starttime_endtime.csv
    """
    if isFirst:
        startTime = BINANCE_START_DATE_STR
    if endTime:
        try:
            endEpochTime = dt2epochTime(endTime)
        except:
            raise ValueError('Cannot convert endTime to epoch time. Time format is "YYYY-mm-dd HH:MM:SS"')
    else:
        endEpochTime = None

    client = Client()
    try:
        pairs_df = pd.read_csv('data/pairs.csv')
    except FileNotFoundError:
        error_instruction = "File data/pairs.csv not found. " + \
            "Run function get_symbol_pairs from pybinance_utils.common, " + \
            "modify is_used column then save to data/pairs.csv file"
        print(error_instruction)
        return
    df_saved_names = []
    symbol_pairs = pairs_df[pairs_df.is_used]['symbol'].values
    for idx, symbol in enumerate(symbol_pairs):
        while True:
            try:
                candles = client.get_historical_klines(symbol=symbol, interval=interval, start_str=startTime, end_str=endEpochTime)
            except requests.exceptions.ReadTimeout as e:
                print(symbol, e)
                continue
            break

        if not candles:
            continue
        save_dir = '{}/{}'.format(data_save_dir, symbol)
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        candles = np.array(candles).astype(float)
        opentimes = []
        closetimes = []
        for candle in candles:
            opentimes.append(epochTime2dt(candle[0]))
            closetimes.append(epochTime2dt(candle[6]))

        df = pd.DataFrame({
            'Opentime': opentimes, 'Closetime': closetimes,
            'Openprice': candles[:, 1], 'Closeprice': candles[:, 4],
            'High': candles[:, 2], 'Low': candles[:, 3], 'Volume': candles[:, 5],
            'Quote asset volume': candles[:, 7], 'Number of trades': candles[:, 8]
        })
        save_name = '{}/{}_{}_{}.csv'.format(save_dir, interval,
                                             opentimes[0].replace('-', '').replace(':', '').replace(' ', '-'),
                                             closetimes[-1].replace('-', '').replace(':', '').replace(' ', '-'))
        df.to_csv(save_name, index=False)
        df_saved_names.append(save_name)

        if (idx+1)%1000 == 0:
            print('Retrieved {} symbols. Waiting for 1 minute before continue'.format(idx+1))
            time.sleep(60)

    return df_saved_names

def get_specific_pair_prices(symbol:str, interval:str, startTime:str, endTime:str=None, isFirst:bool=False) -> pd.DataFrame:
    """Get candlestick price of specific pair

    Args:
        symbol (str): Symbol pair to get.
        interval (str): Timeframe of each candle.
        startTime (str): Start time to get data (Recommend format YYYY-mm-dd). Data will be retrieved from 07:00:00 of that date
        endTime (str, optional): End time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None to get until now
        isFirst (bool, optional): Use when get data for the first time. startTime is set to 2017-01-01 00:00:00 if this param is True. Defaults to False.

    Returns:
        pd.DataFrame: DataFrame with shape (limit, 9) of price of symbol
    """
    if isFirst:
        startTime = BINANCE_START_DATE_STR
    if endTime:
        try:
            endEpochTime = dt2epochTime(endTime)
        except:
            raise ValueError('Cannot convert endTime to epoch time. Time format is "YYYY-mm-dd HH:MM:SS"')
    else:
        endEpochTime = None

    client = Client()
    candles = client.get_historical_klines(symbol=symbol, interval=interval, start_str=startTime, end_str=endEpochTime)
    if not candles:
        return pd.DataFrame()
    candles = np.array(candles).astype(float)
    opentimes = []
    closetimes = []
    for candle in candles:
        opentimes.append(epochTime2dt(candle[0]))
        closetimes.append(epochTime2dt(candle[6]))

    df = pd.DataFrame({
        'Opentime': opentimes, 'Closetime': closetimes,
        'Openprice': candles[:, 1], 'Closeprice': candles[:, 4],
        'High': candles[:, 2], 'Low': candles[:, 3], 'Volume': candles[:, 5],
        'Quote asset volume': candles[:, 7], 'Number of trades': candles[:, 8]
    })
    return df

def get_saving_products(api_key:str, api_secret:str) -> pd.DataFrame:
    """Get list of saving advertising

    Args:
        api_key (str): API key generated from Binance
        api_secret (str): API secret generated from Binance

    Returns:
        pd.DataFrame: DataFrame of saving advertising
    """
    client = Client(api_key, api_secret)
    product_list = []
    current_page = 1
    response = client.get_lending_product_list(current=current_page)
    while len(response) > 0:
        for product in response:
            product_list.append((
                product['asset'], product['avgAnnualInterestRate'], product['dailyInterestPerThousand'],
                product['purchasedAmount'], product['upLimit'], product['canPurchase'],
                product['upLimitPerUser'], product['minPurchaseAmount']
            ))
        current_page += 1
        response = client.get_lending_product_list(current=current_page)
    
    df = pd.DataFrame(product_list, columns=['asset', '7day_apy_interest', 'daily_interestPerThousand',
                                             'purchased_amount', 'limit', 'can_purchase',
                                             'limitPerUser', 'min_amount'])
    df[['7day_apy_interest', 'daily_interestPerThousand', 'purchased_amount',
        'limit', 'limitPerUser', 'min_amount']] = df[['7day_apy_interest', 'daily_interestPerThousand', 'purchased_amount',
                                                      'limit', 'limitPerUser', 'min_amount']].astype(float)
    return df
