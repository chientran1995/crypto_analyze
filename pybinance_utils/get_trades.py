import time
import requests
import numpy as np
import pandas as pd

from binance import Client

from .common import epochTime2dt, dt2epochTime, days2epoch


max_epochtime_diff = days2epoch(90) # max window size is 90 days
day_epochtime_diff = days2epoch(1)

def get_user_trades(api_key:str, api_secret:str, startTime:str=None, endTime:str=None) -> pd.DataFrame:
    """Get history of user trades (executed orders)

    Args:
        api_key (str): API key generated from Binance
        api_secret (str): API secret generated from Binance
        startTime (str, optional): Start time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None
        endTime (str, optional): End time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None to get until now

    Returns:
        pd.DataFrame: DataFrame of user trades history
    """
    if startTime:
        try:
            startEpochTime = dt2epochTime(startTime)
        except:
            raise ValueError('Cannot convert startTime to epoch time. Time format is "YYYY-mm-dd HH:MM:SS"')
    else:
        startEpochTime = None
    if endTime:
        try:
            endEpochTime = dt2epochTime(endTime)
        except:
            raise ValueError('Cannot convert endTime to epoch time. Time format is "YYYY-mm-dd HH:MM:SS"')
    else:
        endEpochTime = None

    if startEpochTime and endEpochTime and endEpochTime - startEpochTime > day_epochtime_diff:
        time_list = [startEpochTime]
        while time_list[-1] + day_epochtime_diff < endEpochTime:
            time_list.append(time_list[-1] + day_epochtime_diff)
        time_list.append(endEpochTime)
    else:
        time_list = [startEpochTime, endEpochTime]

    client = Client(api_key, api_secret)
    try:
        pairs_df = pd.read_csv('data/pairs.csv')
    except FileNotFoundError:
        error_instruction = "File data/pairs.csv not found. " + \
            "Run function get_symbol_pairs from pybinance_utils.common, " + \
            "modify is_used column then save to data/pairs.csv file"
        print(error_instruction)
        return
    trade_list = []
    symbol_pairs = pairs_df[pairs_df.is_used]['symbol'].values
    for idx, sym in enumerate(symbol_pairs):
        for time_idx in range(len(time_list) - 1):
            trades = client.get_my_trades(symbol=sym, startTime=time_list[time_idx], endTime=time_list[time_idx+1])
            for trade in trades:
                trade_list.append((trade['symbol'], 'BUY' if trade['isBuyer']  else 'SELL',
                                trade['price'], trade['qty'], trade['quoteQty'],
                                trade['commission'], trade['commissionAsset'], epochTime2dt(trade['time'])))
            if ((idx+1)*(time_idx+1))%110 == 0:
                print('Retrieved {} trades. Waiting for 1 minute before continue'.format(idx+1))
                time.sleep(60)

    df = pd.DataFrame(trade_list, columns=['symbol', 'type', 'price', 'amount', 'total',
                                           'fee', 'fee_asset', 'time'])
    df[['price', 'amount', 'total', 'fee']] = df[['price', 'amount', 'total', 'fee']].astype(float)

    agg_funcs = {'type':'first', 'price':'mean', 'amount':'sum', 'total':'sum', 'fee':'sum', 'fee_asset':'first'}
    df = df.groupby(['time', 'symbol']).agg(agg_funcs).reset_index()
    return df.sort_values('time')

def get_user_deposit(api_key:str, api_secret:str, startTime:str, endTime:str=None) -> pd.DataFrame:
    """Get deposit history of user

    Args:
        api_key (str): API key generated from Binance
        api_secret (str): API secret generated from Binance
        startTime (str): Start time to get data with format YYYY-mm-dd HH:MM:SS.
        endTime (str, optional): End time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None to get until now

    Raises:
        ValueError: startTime must be smaller than current time

    Returns:
        pd.DataFrame: DataFrame of user deposit history
    """
    client = Client(api_key, api_secret)
    current_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    current_epoch_time = dt2epochTime(current_time)
    start_time = dt2epochTime(startTime)
    if start_time >= current_epoch_time:
        raise ValueError('startTime must be smaller than current time')
    if endTime:
        end_time = dt2epochTime(endTime)
        if end_time - start_time > max_epochtime_diff:
            end_times = [start_time]
            while end_times[-1] < end_time:
                end_times.append(end_times[-1] + max_epochtime_diff)
        else:
            end_times = [start_time, end_time]
    else:
        end_times = [start_time]
        while end_times[-1] < current_epoch_time:
            end_times.append(end_times[-1] + max_epochtime_diff)
    
    deposit_list = []
    for time_idx in range(len(end_times) - 1):
        transactions = client.get_deposit_history(startTime=end_times[time_idx], endTime=end_times[time_idx+1])
        for tr in transactions:
            deposit_list.append((tr['coin'], tr['amount'], tr['status'], epochTime2dt(tr['insertTime']),
                                 tr['network'], tr['address']))

    df = pd.DataFrame(deposit_list, columns=['coin', 'amount', 'status', 'time', 'network', 'address'])
    return df

def get_saving_purchase(api_key:str, api_secret:str, lendingType:str=None,
                        startTime:str='2017-01-01 00:00:00', endTime:str=None) -> pd.DataFrame:
    """Get history of saving purchases

    Args:
        api_key (str): API key generated from Binance
        api_secret (str): API secret generated from Binance
        lendingType (str, optional): Types of saving in [DAILY, ACTIVITY, CUSTOMIZED_FIXED]. Defaults to None to get all types.
        startTime (str, optional): Start time to get history in format %Y-%m-%d %H:%M:%S. Defaults to '2017-01-01 00:00:00'.
        endTime (str, optional): End time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None to get until now

    Returns:
        pd.DataFrame: DataFrame of saving purchases history
    """
    client = Client(api_key, api_secret)
    purchases_list = []
    lending_types = [lendingType] if lendingType else ['DAILY', 'ACTIVITY', 'CUSTOMIZED_FIXED']
    start_time = dt2epochTime(startTime)
    if endTime:
        try:
            endEpochTime = dt2epochTime(endTime)
        except:
            raise ValueError('Cannot convert endTime to epoch time. Time format is "YYYY-mm-dd HH:MM:SS"')
    else:
        endEpochTime = None
    for lending_type in lending_types:
        current_page = 1
        purchases = client.get_lending_purchase_history(lendingType=lending_type, startTime=start_time,
                                                        endTime=endEpochTime, current=current_page)
        while len(purchases) > 0:
            for purchase in purchases:
                purchases_list.append((purchase['productName'], purchase['asset'], purchase['amount'],
                                       purchase['status'], purchase['lendingType'], epochTime2dt(purchase['createTime'])))
            current_page += 1
            purchases = client.get_lending_purchase_history(lendingType=lending_type, startTime=start_time,
                                                            endTime=endEpochTime, current=current_page)

    df = pd.DataFrame(purchases_list, columns=['product_name', 'asset', 'amount',
                                               'status', 'lending_type', 'create_time'])
    return df

def get_saving_redemption(api_key:str, api_secret:str, lendingType:str=None,
                          startTime:str='2017-01-01 00:00:00', endTime:str=None) -> pd.DataFrame:
    """Get history of saving redemptions

    Args:
        api_key (str): API key generated from Binance
        api_secret (str): API secret generated from Binance
        lendingType (str, optional): Types of saving in [DAILY, ACTIVITY, CUSTOMIZED_FIXED]. Defaults to None to get all types.
        startTime (str, optional): Start time to get history in format %Y-%m-%d %H:%M:%S. Defaults to '2017-01-01 00:00:00'.
        endTime (str, optional): End time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None to get until now

    Returns:
        pd.DataFrame: DataFrame of saving redemption history
    """
    client = Client(api_key, api_secret)
    purchases_list = []
    lending_types = [lendingType] if lendingType else ['DAILY', 'ACTIVITY', 'CUSTOMIZED_FIXED']
    start_time = dt2epochTime(startTime)
    if endTime:
        try:
            endEpochTime = dt2epochTime(endTime)
        except:
            raise ValueError('Cannot convert endTime to epoch time. Time format is "YYYY-mm-dd HH:MM:SS"')
    else:
        endEpochTime = None
    for lending_type in lending_types:
        current_page = 1
        purchases = client.get_lending_redemption_history(lendingType=lending_type, startTime=start_time,
                                                          endTime=endEpochTime, current=current_page)
        while len(purchases) > 0:
            for purchase in purchases:
                purchases_list.append((purchase['projectName'], purchase['asset'], purchase['amount'], purchase['principal'],
                                    purchase['status'], purchase['type'], epochTime2dt(purchase['createTime'])))
            current_page += 1
            purchases = client.get_lending_redemption_history(lendingType=lending_type, startTime=start_time,
                                                              endTime=endEpochTime, current=current_page)

    df = pd.DataFrame(purchases_list, columns=['product_name', 'asset', 'amount', 'principal',
                                               'status', 'type', 'create_time'])
    return df
