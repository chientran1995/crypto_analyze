import time
import numpy as np
import pandas as pd

from binance import Client


client = Client()

def days2epoch(d:float=90) -> int:
    """Return epoch time from number of days. Usually use for time delta.

    Args:
        d (float, optional): Number of days. Defaults to 90 days.

    Returns:
        int: Epoch time
    """
    return d*24*60*60*1000

def epoch2days(ep:int) -> int:
    """Return number of days from epoch. Usually use for time delta.

    Args:
        ep (int): Epoch time

    Returns:
        int: Number of days
    """
    return ep//(24*60*60*1000)

def epochTime2dt(epoch_time:int) -> str:
    """Return datetime in format %Y-%m-%d %H:%M:%S from epoch time

    Args:
        epoch_time (int): Epoch time

    Returns:
        str: datetime in format %Y-%m-%d %H:%M:%S
    """
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(epoch_time//1000))

def dt2epochTime(dt:str) -> int:
    """Return epoch time from datetime string in format %Y-%m-%d %H:%M:%S

    Args:
        dt (str): Datetime string in format %Y-%m-%d %H:%M:%S

    Returns:
        int: Epoch time
    """
    return int(time.mktime(time.strptime(dt, '%Y-%m-%d %H:%M:%S'))*1000)

def get_symbol_pairs(default_use=False) -> pd.DataFrame:
    """Get all symbol pairs from exchange

    Args:
        default_use (bool, optional): Auto check is used for USDT, BUSD, BNB markets. Defaults to False.

    Returns:
        pd.DataFrame: DataFrame of symbol pairs
    """
    pairs = []
    exchange = client.get_exchange_info()
    for sym in exchange['symbols']:
        pairs.append((sym['symbol'], sym['baseAsset'], sym['quoteAsset']))
    df = pd.DataFrame(pairs, columns =['symbol', 'base_asset', 'quote_asset'])
    if default_use:
        df['is_used'] = ((df['quote_asset'] == 'USDT') | (df['quote_asset'] == 'BUSD') | (df['quote_asset'] == 'BNB'))
    else:
        df['is_used'] = False
    return df
