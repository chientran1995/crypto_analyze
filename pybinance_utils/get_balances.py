import time
import pandas as pd

from binance import Client

from .common import epochTime2dt, dt2epochTime


def get_earn_balance(api_key:str, api_secret:str) -> pd.DataFrame:
    """Get earn balance of account

    Args:
        api_key (str): API key generated from Binance
        api_secret (str): API secret generated from Binance

    Returns:
        pd.DataFrame: Dataframe of earn balance
    """
    client = Client(api_key, api_secret)
    response = client.get_lending_position()
    earn_wallet = response
    assets = []
    for ass in earn_wallet:
        assets.append((ass['asset'], ass['productName'], ass['totalAmount'],
                       ass['freeAmount'], ass['lockedAmount'], ass['freezeAmount'],
                       ass['redeemingAmount'], ass['totalInterest']))
    df = pd.DataFrame(assets, columns=['asset', 'product_name', 'amount',
                                        'free_amount', 'locked_amount', 'freeze_amount',
                                        'redeeming_amount', 'total_interest'])
    return df

def get_earn_interest_history(api_key:str, api_secret:str, lendingType:str=None,
                              startTime:str=None, endTime:str=None) -> pd.DataFrame:
    """Get interest history

    Args:
        api_key (str): API key generated from Binance
        api_secret (str): API secret generated from Binance
        lendingType (str, optional): Types of saving in [DAILY, ACTIVITY, CUSTOMIZED_FIXED]. Defaults to None to get all types.
        startTime (str, optional): Start time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None.
        endTime (str, optional): End time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None.

    Raises:
        ValueError: startTime not in format "YYYY-mm-dd HH:MM:SS"
        ValueError: endTime not in format "YYYY-mm-dd HH:MM:SS"

    Returns:
        pd.DataFrame: DataFrame of history of interest from saving
    """
    # DAILY = flexible, ACTIVITY = activity, CUSTOMIZED_FIXED = fixed
    lending_types = [lendingType] if lendingType else ['DAILY', 'ACTIVITY', 'CUSTOMIZED_FIXED']
    if startTime:
        try:
            startEpochTime = dt2epochTime(startTime)
        except:
            raise ValueError('Cannot convert startTime to epoch time. Time format is "YYYY-mm-dd HH:MM:SS"')
    else:
        startEpochTime = None
    if endTime:
        try:
            endEpochTime = dt2epochTime(endTime)
        except:
            raise ValueError('Cannot convert endTime to epoch time. Time format is "YYYY-mm-dd HH:MM:SS"')
    else:
        endEpochTime = None
    interest_rows = []
    client = Client(api_key, api_secret)
    for lending_type in lending_types:
        current_page = 1
        interests = client.get_lending_interest_history(lendingType=lending_type, startTime=startEpochTime,
                                                        endTime=endEpochTime, current=current_page, size=100)
        while len(interests) > 0:
            for interest in interests:
                interest_rows.append((interest['asset'], interest['productName'], interest['interest'],
                                      interest['lendingType'], epochTime2dt(interest['time'])))
            current_page += 1
            interests = client.get_lending_interest_history(lendingType=lending_type, startTime=startEpochTime,
                                                            endTime=endEpochTime, current=current_page, size=100)

    df = pd.DataFrame(interest_rows, columns=['asset', 'product_name', 'interest', 'lending_type', 'datetime'])
    return df

def get_spot_balance(api_key:str, api_secret:str) -> pd.DataFrame:
    """Get spot balance of account

    Args:
        api_key (str): API key generated from Binance
        api_secret (str): API secret generated from Binance

    Returns:
        pd.DataFrame: Dataframe of spot balance
    """
    client = Client(api_key, api_secret)
    response = client.get_account()
    spot_wallet = response['balances']
    assets = []
    for ass in spot_wallet:
        if float(ass['free']) != 0 or float(ass['locked']) != 0:
            assets.append((ass['asset'], ass['free'], ass['locked']))
    df = pd.DataFrame(assets, columns=['asset', 'free', 'locked'])
    return df
