import time
import pandas as pd

from binance import Client

from .common import epochTime2dt, dt2epochTime, days2epoch


day_epochtime_diff = days2epoch(1)

def get_user_orders(api_key:str, api_secret:str, startTime:str=None, endTime:str=None) -> pd.DataFrame:
    """Get history of all user orders

    Args:
        api_key (str): API key generated from Binance
        api_secret (str): API secret generated from Binance
        startTime (str, optional): Start time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None
        endTime (str, optional): End time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None to get until now

    Returns:
        pd.DataFrame: DataFrame of user orders history
    """
    if startTime:
        try:
            startEpochTime = dt2epochTime(startTime)
        except:
            raise ValueError('Cannot convert startTime to epoch time. Time format is "YYYY-mm-dd HH:MM:SS"')
    else:
        startEpochTime = None
    if endTime:
        try:
            endEpochTime = dt2epochTime(endTime)
        except:
            raise ValueError('Cannot convert endTime to epoch time. Time format is "YYYY-mm-dd HH:MM:SS"')
    else:
        endEpochTime = None

    if startEpochTime and endEpochTime and endEpochTime - startEpochTime > day_epochtime_diff:
        time_list = [startEpochTime]
        while time_list[-1] + day_epochtime_diff < endEpochTime:
            time_list.append(time_list[-1] + day_epochtime_diff)
        time_list.append(endEpochTime)
    else:
        time_list = [startEpochTime, endEpochTime]

    client = Client(api_key, api_secret)
    try:
        pairs_df = pd.read_csv('data/pairs.csv')
    except FileNotFoundError:
        error_instruction = "File data/pairs.csv not found. " + \
            "Run function get_symbol_pairs from pybinance_utils.common, " + \
            "modify is_used column then save to data/pairs.csv file"
        print(error_instruction)
        return
    order_list = []
    symbol_pairs = pairs_df[pairs_df.is_used]['symbol'].values
    for idx, sym in enumerate(symbol_pairs):
        for time_idx in range(len(time_list) - 1):
            orders = client.get_all_orders(symbol=sym, startTime=time_list[time_idx], endTime=time_list[time_idx+1])
            for order in orders:
                order_list.append((order['symbol'], order['type'], order['side'], order['stopPrice'],
                                order['price'], order['origQty'], order['executedQty'], order['cummulativeQuoteQty'],
                                order['status'], epochTime2dt(order['time'])))
            if ((idx+1)*(time_idx+1))%110 == 0:
                print('Retrieved {} orders. Waiting for 1 minute before continue'.format(idx+1))
                time.sleep(60)

    df = pd.DataFrame(order_list, columns=['symbol', 'order_type', 'type', 'stop_price', 'price',
                                           'amount', 'executed_amount', 'total', 'status', 'time'])
    df[['stop_price', 'price', 'amount', 'executed_amount', 'total']] = \
        df[['stop_price', 'price', 'amount', 'executed_amount', 'total']].astype(float)
    return df.sort_values('time')

def get_user_open_orders(api_key:str, api_secret:str) -> pd.DataFrame:
    """Get all opening orders of user

    Args:
        api_key (str): API key generated from Binance
        api_secret (str): API secret generated from Binance

    Returns:
        pd.DataFrame: DataFrame of user opening orders
    """
    client = Client(api_key, api_secret)
    order_list = []
    orders = client.get_open_orders()
    for order in orders:
        order_list.append((order['symbol'], order['type'], order['side'], order['stopPrice'],
                           order['price'], order['origQty'], order['executedQty'], order['cummulativeQuoteQty'],
                           order['status'], order['isWorking'],
                           epochTime2dt(order['time']), epochTime2dt(order['updateTime'])))

    df = pd.DataFrame(order_list, columns=['symbol', 'order_type', 'type', 'stop_price', 'price',
                                           'amount', 'executed_amount', 'total',
                                           'status', 'is_working', 'time', 'updated_time'])
    df[['stop_price', 'price', 'amount', 'executed_amount', 'total']] = \
        df[['stop_price', 'price', 'amount', 'executed_amount', 'total']].astype(float)
    return df

### Binance API document: https://binance-docs.github.io/apidocs/spot/en/#current-open-orders-user_data
### states "Careful when accessing this with no symbol".
### So use this function to get open orders instead if error happens
# def get_user_open_orders(api_key:str, api_secret:str):
#     client = Client(api_key, api_secret)
#     try:
#         pairs_df = pd.read_csv('data/pairs.csv')
#     except FileNotFoundError:
#         error_instruction = "File data/pairs.csv not found. " + \
#             "Run function get_symbol_pairs from pybinance_utils.common, " + \
#             "modify is_used column then save to data/pairs.csv file"
#         print(error_instruction)
#         return
#     symbol_pairs = pairs_df[pairs_df.is_used]['symbol'].values
#     order_list = []
#     for idx, sym in enumerate(symbol_pairs):
#         orders = client.get_all_orders(symbol=sym)
#         for order in orders:
#             order_list.append((order['symbol'], order['type'], order['side'], order['stopPrice'],
#                                order['price'], order['origQty'], order['executedQty'], order['cummulativeQuoteQty'],
#                                order['status'], order['isWorking'],
#                                epochTime2dt(order['time']), epochTime2dt(order['updateTime'])))
#         if (idx+1)%380 == 0:
#             print('Retrieved {} orders. Waiting for 1 minute before continue'.format(idx+1))
#             time.sleep(60)

#     df = pd.DataFrame(order_list, columns=['symbol', 'order_type', 'type', 'stop_price', 'price',
#                                            'amount', 'executed_amount', 'total',
#                                            'status', 'is_working', 'time', 'updated_time'])
#     df[['stop_price', 'price', 'amount', 'executed_amount', 'total']] = \
#         df[['stop_price', 'price', 'amount', 'executed_amount', 'total']].astype(float)
#     return df
