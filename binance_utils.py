import time
import requests
import numpy as np
import pandas as pd


ENDPOINTS = {
    'PAIR_SYMBOLS_LIST': 'https://api.binance.com/api/v3/exchangeInfo',
    'SYMBOL_PRICE': 'https://www.binance.com/api/v3/klines?symbol={}&interval={}&limit={}',
}

def get_binance_pair(ref_coin:str=None) -> list:
    """Functions to get all pair or based on reference coin from Binance

    Args:
        ref_coin (str, optional): Reference coin. Defaults to None to get all pairs.

    Returns:
        list: list of pair codes
    """
    response = requests.get(ENDPOINTS['PAIR_SYMBOLS_LIST'])
    jres = response.json()
    syms = jres['symbols']

    lst_symbols = []
    if ref_coin:
        for sym in syms:
            if sym['symbol'][-len(ref_coin):] == ref_coin.upper():
                lst_symbols.append(sym['symbol'])
        return lst_symbols
    else:
        for sym in syms:
            lst_symbols.append(sym['symbol'])
        return lst_symbols

def get_binance_price(symbol:str, interval:str='1d', limit:int=1000, startTime:str=None, endTime:str=None) -> pd.DataFrame:
    """Get candlestick price of symbol from Binance exchange

    Args:
        symbol (str): Symbol of coin pair
        interval (str, optional): Timeframe of each candle. Defaults to '1d'.
        limit (int, optional): Number of candle to get. Defaults to 1000. Max 1000.
        startTime (str, optional): Start time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None.
        endTime (str, optional): End time to get data with format YYYY-mm-dd HH:MM:SS. Defaults to None.

    Returns:
        pd.DataFrame: DataFrame with shape (limit, 9) of price of symbol
    """
    assert limit <= 1000
    assert interval in ['1m', '3m', '5m', '15m', '30m',
                        '1h', '2h', '4h', '6h', '8h', '12h',
                        '1d', '3d', '1w', '1M']
    
    symbol = symbol.upper()
    endpoint = ENDPOINTS['SYMBOL_PRICE'].format(symbol, interval, limit)
    if startTime:
        try:
            startEpochTime = time.mktime(time.strptime(startTime, '%Y-%m-%d %H:%M:%S'))*1000
        except:
            raise ValueError('Cannot convert startTime to epoch time. Time format is "YYYY-mm-dd HH:MM:SS"')
        endpoint += '&startTime={}'.format(int(startEpochTime))
    if endTime:
        try:
            endEpochTime = time.mktime(time.strptime(endTime, '%Y-%m-%d %H:%M:%S'))*1000
        except:
            raise ValueError('Cannot convert endTime to epoch time. Time format is "YYYY-mm-dd HH:MM:SS"')
        endpoint += '&endTime={}'.format(int(endEpochTime))

    response = requests.get(endpoint)
    res = response.json()
    res = np.array(res).astype(float)
    opentimes = []
    closetimes = []
    for candle in res:
        opentimes.append(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(candle[0]//1000)))
        closetimes.append(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(candle[6]//1000)))
    df = pd.DataFrame({
        'Opentime': opentimes, 'Closetime': closetimes,
        'Openprice': res[:, 1], 'Closeprice': res[:, 4],
        'High': res[:, 2], 'Low': res[:, 3], 'Volume': res[:, 5],
        'Quote asset volume': res[:, 7], 'Number of trades': res[:, 8]
    })
    return df
