import os
import yaml
import time
import argparse

from pybinance_utils import common, get_balances, get_orders, get_trades, get_market_prices


parser = argparse.ArgumentParser(description='Process arguments.')
parser.add_argument('-conf', '--config', type=str, required=True, help='Config file with api key and api secret.')
parser.add_argument('-first', '--isFirst', action='store_true',
                    help='Use when get data for the first time. start_time is set to 2017-01-01 00:00:00 if this param is specified')
parser.add_argument('-interval', '--interval', type=str, required=True, help='Interval to get candlestick data')
parser.add_argument('-stime', '--startTime', type=str, required=False, help='Start time to get data with format YYYY-mm-dd HH:MM:SS')
parser.add_argument('-etime', '--endTime', type=str, required=True, help='End time to get data with format YYYY-mm-dd HH:MM:SS')
args = parser.parse_args()

def main(args):
    config_file = args.config
    first_run = args.isFirst
    interval = args.interval
    if first_run:
        start_time = '2017-01-01 00:00:00'
    else:
        start_time = args.startTime
        if not start_time:
            print('startTime is required if isFirst is False')
            return
    end_time = args.endTime
    
    with open(config_file, 'r') as conf_file:
        cfg = yaml.load(conf_file, Loader=yaml.FullLoader)
    api_key, api_secret = cfg['API']['api_key'], cfg['API']['api_secret']

    ### Get market data
    market_data_dir = 'data/market_data'
    if not os.path.exists(market_data_dir):
        os.makedirs(market_data_dir)

    saving_advertisements_df = get_market_prices.get_saving_products(api_key, api_secret)
    saving_advertisements_df.to_csv('{}/market_saving_advertisements.csv'.format(market_data_dir), index=False)

    # intervals are '30m', '1h', '4h', '12h', '1d', '1w', '1M'
    get_market_prices.get_all_pair_prices('{}/market_binance'.format(market_data_dir), interval=interval,
                                          startTime=start_time, endTime=end_time)

if __name__ == '__main__':
    main(args)
    