import sys
sys.path.append('..')
import time
import requests

from pybinance_utils.common import dt2epochTime, days2epoch


def round_time_updown(tstring:str) -> tuple:
    """Ceiling and floor round a time string

    Args:
        tstring (str): String of time in format YYYY-mm-dd HH:MM:SS

    Returns:
        tuple: (start epoch time, end epoch time)
    """
    ttime = time.strptime(tstring, '%Y-%m-%d %H:%M:%S')
    ftime = dt2epochTime(time.strftime('%Y-%m-%d %H:00:00', ttime))
    return int(ftime), int(ftime + days2epoch(1/24.))

def get_coin_usdt_price(asset:str, time_string:str=None) -> float:
    """Get usdt price of an asset at specific time

    Args:
        asset (str): Abbreviation of asset
        time_string (str, optional): Time to get the price in format YYYY-mm-dd HH:MM:SS. Defaults to None to get current price.

    Returns:
        float: Price of an asset at specific time
    """
    if asset == 'USDT':
        return 1.0
    endpoint = 'https://www.binance.com/api/v3/klines?symbol={}USDT&interval=1h'.format(asset)
    if time_string:
        startEpochTime, endEpochTime = round_time_updown(time_string)
        endpoint += '&startTime={}&endTime={}'.format(startEpochTime, endEpochTime)
    try:
        response = requests.get(endpoint)
        res = response.json()
        last_price = float(res[-1][4])
    except Exception as ex:
        print(asset, time_string, ex)
        last_price = 0.0001
    return last_price

def process_trade_gain_cost(trade):
    fee_cost = trade['fee'] * get_coin_usdt_price(trade['fee_asset'], trade['time'])
    if trade['type'] == 'BUY':
        trade['gain_usdt'] = trade['amount'] * get_coin_usdt_price(trade['base_asset'], trade['time'])
        trade['cost_usdt'] = trade['total'] * get_coin_usdt_price(trade['quote_asset'], trade['time'])
    elif trade['type'] == 'SELL':
        trade['gain_usdt'] = trade['total'] * get_coin_usdt_price(trade['quote_asset'], trade['time'])
        trade['cost_usdt'] = trade['amount'] * get_coin_usdt_price(trade['base_asset'], trade['time'])
    trade['cost_usdt'] += fee_cost
    return trade
