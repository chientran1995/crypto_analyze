import sys
sys.path.append('..')
import os
import time
import requests
import numpy as np
import pandas as pd
from collections import defaultdict
from sortedcontainers import SortedDict

from utils_management import get_coin_usdt_price


def process_asset_trades(asset_df, asset):
    summary = []
    asset_available_dict = SortedDict({})
    for _, trade in asset_df.iterrows():
        if trade['base_asset'] == asset:
            if trade['type'] == 'BUY':
                # Add to available
                usdt_price = trade['price'] * get_coin_usdt_price(trade['quote_asset'], trade['time'])
                usdt_price += trade['fee'] * get_coin_usdt_price(trade['fee_asset'], trade['time'])
                asset_available_dict[usdt_price] = trade['amount']
                summary.append(None)
            elif trade['type'] == 'SELL':
                amount, price = trade['amount'], trade['price'] * get_coin_usdt_price(trade['quote_asset'], trade['time'])
                gain = 0.0
                while amount > 0:
                    if not asset_available_dict:
                        break
                    min_price = asset_available_dict.keys()[0]
                    min_price_amount = asset_available_dict[min_price]
                    fill_amount = min(amount, min_price_amount)
                    gain += fill_amount * (price - min_price)
                    amount -= fill_amount
                    asset_available_dict[min_price] -= fill_amount
                    if asset_available_dict[min_price] <= 0:
                        asset_available_dict.pop(min_price)
                gain -= trade['fee'] * get_coin_usdt_price(trade['fee_asset'], trade['time'])
                summary.append(gain)
        else:
            if trade['type'] == 'BUY':
                amount, price = trade['total'], (1./trade['price']) * get_coin_usdt_price(trade['base_asset'], trade['time'])
                gain = 0.0
                while amount > 0:
                    if not asset_available_dict:
                        break
                    min_price = asset_available_dict.keys()[0]
                    min_price_amount = asset_available_dict[min_price]
                    fill_amount = min(amount, min_price_amount)
                    gain += fill_amount * (price - min_price)
                    amount -= fill_amount
                    asset_available_dict[min_price] -= fill_amount
                    if asset_available_dict[min_price] <= 0:
                        asset_available_dict.pop(min_price)
                gain -= trade['fee'] * get_coin_usdt_price(trade['fee_asset'], trade['time'])
                summary.append(gain)
            elif trade['type'] == 'SELL':
                # Add to available
                usdt_price = (1./trade['price']) * get_coin_usdt_price(trade['base_asset'], trade['time'])
                usdt_price += trade['fee'] * get_coin_usdt_price(trade['fee_asset'], trade['time'])
                asset_available_dict[usdt_price] = trade['total']
                summary.append(None)
    return summary, asset_available_dict

def main():
    pair_df = pd.read_csv('../data/pairs.csv')
    trade_hist = pd.read_csv('../data/personal_data/trade_history_spot.csv')
    trade_hist = trade_hist.merge(pair_df[['symbol', 'base_asset', 'quote_asset']], on='symbol', how='left')

    asset_used = sorted(list(set(trade_hist['base_asset'].tolist() + trade_hist['quote_asset'].tolist())))
    available_df = []
    for asset in asset_used:
        if 'USD' in asset:
            continue
        coin_df = trade_hist[trade_hist['symbol'].str.contains(asset)].sort_values(by='time')
        trade_summary_for_asset, asset_available = process_asset_trades(coin_df, asset)

        if any(trade_summary_for_asset):
            trade_hist.loc[coin_df.index, '{}_summary'.format(asset)] = trade_summary_for_asset

        asset_current_price = get_coin_usdt_price(asset)
        for pricekey in asset_available:
            trade_assumption_profit = (asset_current_price - pricekey) * asset_available[pricekey]
            trade_assumption_profit_perc = 100 * ((asset_current_price - pricekey) / pricekey)
            recovery_percent = None
            if trade_assumption_profit < 0:
                recovery_percent = 100*((pricekey - asset_current_price)/asset_current_price)
            available_df.append((asset, asset_available[pricekey], pricekey, asset_current_price,
                                 trade_assumption_profit, trade_assumption_profit_perc, recovery_percent))

    df = pd.DataFrame(available_df, columns=['asset', 'amount', 'capital_usdt', 'current_price',
                                             'assumption_profit', 'assumption_profit_perc', 'price_recovery_percent'])
    df[['amount', 'capital_usdt', 'current_price', 'assumption_profit',
        'assumption_profit_perc', 'price_recovery_percent']] = \
        df[['amount', 'capital_usdt', 'current_price', 'assumption_profit',
            'assumption_profit_perc', 'price_recovery_percent']].astype(float)
    return trade_hist, df

if __name__ == '__main__':
    trade_sell_summary_df, available_trades_df = main()
    trade_sell_summary_df.to_csv('../data/personal_data/derived/trade_sell_summary.csv', index=False)
    available_trades_df.to_csv('../data/personal_data/derived/trade_available_summary.csv', index=False)
