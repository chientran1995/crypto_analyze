import pandas as pd

from utils_management import get_coin_usdt_price

def get_saving_interest(saving_hist_path):
    saving_hist = pd.read_csv(saving_hist_path)
    saving_dict = saving_hist[['asset', 'interest']].groupby('asset').sum().to_dict()
    return saving_dict['interest']

def main():
    saving_interest_dict = get_saving_interest('../data/personal_data/balance_saving_history.csv')

    pair_df = pd.read_csv('../data/pairs.csv')
    trade_hist = pd.read_csv('../data/personal_data/trade_history_spot.csv')
    trade_hist = trade_hist.merge(pair_df[['symbol', 'base_asset', 'quote_asset']], on='symbol', how='left')

    asset_summary_dict = []
    asset_used = list(set(trade_hist['base_asset'].tolist() + trade_hist['quote_asset'].tolist()))
    for asset in asset_used:
        total_amount = 0
        total_cost = 0
        coin_df = trade_hist[trade_hist['symbol'].str.contains(asset)]

        for _, trade in coin_df.iterrows():
            if trade['base_asset'] == asset:
                if trade['type'] == 'BUY':
                    total_amount += trade['amount']
                    total_cost += trade['total'] * get_coin_usdt_price(trade['quote_asset'], trade['time'])
                    total_cost += trade['fee'] * get_coin_usdt_price(trade['fee_asset'], trade['time'])
                elif trade['type'] == 'SELL':
                    total_amount -= trade['amount']
                    total_cost -= trade['total'] * get_coin_usdt_price(trade['quote_asset'], trade['time'])
                    total_cost += trade['fee'] * get_coin_usdt_price(trade['fee_asset'], trade['time'])
            else:
                if trade['type'] == 'BUY':
                    total_amount -= trade['total']
                    total_cost -= trade['amount'] * get_coin_usdt_price(trade['base_asset'], trade['time'])
                    total_cost += trade['fee'] * get_coin_usdt_price(trade['fee_asset'], trade['time'])
                elif trade['type'] == 'SELL':
                    total_amount += trade['total']
                    total_cost += trade['amount'] * get_coin_usdt_price(trade['base_asset'], trade['time'])
                    total_cost += trade['fee'] * get_coin_usdt_price(trade['fee_asset'], trade['time'])
        if asset in saving_interest_dict:
            total_amount += saving_interest_dict[asset]
        asset_current_price = get_coin_usdt_price(asset)
        asset_assumption_profit = asset_current_price * total_amount - total_cost
        asset_assumption_profit_perc = 100 * (asset_assumption_profit/total_cost)
        recovery_percent = None
        if asset_assumption_profit < 0:
            recovery_price = total_cost/total_amount
            recovery_percent = 100*((recovery_price - asset_current_price)/asset_current_price)

        asset_summary_dict.append((asset, total_amount, total_cost, total_cost/total_amount, asset_current_price,
                                   asset_assumption_profit, asset_assumption_profit_perc, recovery_percent))

    df = pd.DataFrame(asset_summary_dict, columns=['asset', 'available_amount', 'capital_usdt', 'average_usdt_price',
                                                   'current_price', 'assumption_profit', 'assumption_profit_perc',
                                                   'price_recovery_percent'])
    df[['available_amount', 'capital_usdt', 'average_usdt_price',
        'current_price', 'assumption_profit', 'assumption_profit_perc', 'price_recovery_percent']] = \
        df[['available_amount', 'capital_usdt', 'average_usdt_price',
            'current_price', 'assumption_profit', 'assumption_profit_perc', 'price_recovery_percent']].astype(float)
    return df.sort_values('asset')

if __name__ == '__main__':
    df = main()
    df.to_csv('../data/personal_data/derived/asset_summary.csv', index=False)
